CREATE TABLE item(item_id int primary key,item_name VARCHAR(256) NOT NULL,item_price int NOT NULL,category int NOT NULL);
ALTER TABLE item CHANGE COLUMN category category_id int NOT NULL;
